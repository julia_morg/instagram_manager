from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response


def main_view(request):
    return Response('Hello!')

if __name__ == '__main__':
    config = Configurator()
    config.add_route('main', '')
    config.add_view(main_view, route_name='main')
    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever()
   
